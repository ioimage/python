import random

def readInt(tip):
	isNum = False
	while not isNum:
		isNum = True
		try:
			num = int(input(tip))
		except NameError:
			print "I said a number!"
			isNum = False
		except ValueError:
			print "I said a int number!"
			isNum = False
	return num

secret = int(random.uniform(0,10))
guess = 11

while guess != secret:
	guess = readInt("input 0~10 : ");

print "well done!"
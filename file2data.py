from numpy import *

def file2data(filename):
	fr = open(filename)
	content = fr.readlines()
	nLine = len(content)
	nLineDims = len(content[0].split())
	data  = zeros((nLine,nLineDims-1))
	label = zeros(nLine)

	i = 0
	for line in content:
		line = line.strip()
		dataOfLine = line.split()
		data[i,:] = dataOfLine[1:]
		label[i] = int(dataOfLine[0])
		print dataOfLine
		i += 1
	return data,label

def data2file(data,label,filename):
	fr = open(filename,'w')
	nDataLine = len(data)
	nLabsLine = len(label)
	print nDataLine
	if nDataLine == nLabsLine:
		for i in range(1,nDataLine):
			dbuf = label[i] + data[i]
			fr.write(str(dbuf))
			print dbuf



if __name__ == "__main__":
	data,label = file2data("data.txt")
	print data
	print label
	data2file(data,label,"data_copy.txt")

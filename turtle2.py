import turtle
#create window
window = turtle.Screen()
babbage = turtle.Turtle()

#draw stem and centre
babbage.left(90)
babbage.forward(100)
babbage.right(90)


#set color
babbage.color("red","green")
babbage.begin_fill()
babbage.circle(10)
babbage.end_fill()

babbage.color("blue","black")
#draw all petals
for i in range(1,24):
	babbage.left(15)
	babbage.forward(50)
	babbage.left(157)
	babbage.forward(50)

window.exitonclick()
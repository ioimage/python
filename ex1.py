
tip = "Enter a number: "
inpNum = input(tip)
num = int(inpNum)

for x in xrange(1,10):
	print "%d times %d is %d"%(x,num,x*num)

even = (num % 2) == 0

if even:
	print num, " is even"
else:
	print num, " is odd"
